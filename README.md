# README #

Information about ecommerce project hv store.

### Was finden Sie hier? ###
In diesem Projekt soll Ihnen die E-Commerce Plattform hv-store.de näher bringen.
Basierend auf einem proprietären Shopsystem und mehreren Erweiterungen in Form von Plugins werden in diesem Shop über 20.000 Artikel angeboten.

Eine erweiterte Suche erleichtert den Besuchern der Webseite die Auswahl der gewünschten Produkte.

* HV-Store alles für Haus & Garten und mediterrane Lebensmittel als Living-Example: [https://www.hv-store.de](https://www.hv-store.de)

Bei Rückfragen einfach Kontakt aufnehmen.